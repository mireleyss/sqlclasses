<?php

    include_once "./connect.php";

    global $db;

    if ($_SERVER['REQUEST_METHOD'] == 'POST' and isset($_POST['submit'])) {
        $userName = $_POST['name'];
        $userAge = $_POST['age'];
        $userEmail = $_POST['email'];

        writeData($db, $userName, $userAge, $userEmail);
    }

    function writeData($db, $userName, $userAge, $userEmail) {
        $query = "INSERT INTO user_data(name, age, email) VALUES (?, ?, ?)";

        $stmt = $db->prepare($query);

        $stmt->bind_param('sis', $userName, $userAge, $userEmail);

        if ($stmt->execute()) {
            echo 'Ваши данные были успешно записаны';
        } else {
            echo 'Ошибка при регистрации ' . $db->error;
        }

        $stmt->close();
    }