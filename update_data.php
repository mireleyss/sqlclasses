<?php

    global $db;
    session_start();
    if ($_SERVER['REQUEST_METHOD'] == 'POST' and isset($_POST['change_data'])) {
        $userID = $_POST['id'];
        $_SESSION['userID'] = $userID;
        include_once "./update_form.php";
    }

    $cachedUserID = $_SESSION['userID'];

    switch(true) {
        case $_SERVER['REQUEST_METHOD'] == 'POST' and isset($_POST['submit_name']):

            $changedName = $_POST['update_name'];
            changeName($db, $cachedUserID, $changedName);
            break;

        case $_SERVER['REQUEST_METHOD'] == 'POST' and isset($_POST['submit_age']):

            $changedAge = $_POST['update_age'];
            changeAge($db, $cachedUserID, $changedAge);
            break;

        case $_SERVER['REQUEST_METHOD'] == 'POST' and isset($_POST['submit_email']):

            $changedEmail = $_POST['update_email'];
            changeEmail($db, $cachedUserID, $changedEmail);
            break;
    }


    function changeName($db, $userID, $userName) {
        $query = "UPDATE user_data SET name = ? WHERE id = ?";
        $stmt = $db->prepare($query);

        $stmt->bind_param('si', $userName, $userID);

        if ($stmt->execute()) {
            echo 'Ваши данные были успешно изменены';;
        } else echo 'Ошибка' . $db->error;

        $stmt->close();
    }

    function changeAge($db, $userID, $userAge) {
        $query = "UPDATE user_data SET age = ? WHERE id = ?";
        $stmt = $db->prepare($query);

        $stmt->bind_param('si', $userAge, $userID);

        if ($stmt->execute()) {
            echo 'Ваши данные были успешно изменены';;
        } else echo 'Ошибка' . $db->error;

        $stmt->close();
    }

    function changeEmail($db, $userID, $userEmail) {
        $query = "UPDATE user_data SET email = ? WHERE id = ?";
        $stmt = $db->prepare($query);

        $stmt->bind_param('si', $userEmail, $userID);

        if ($stmt->execute()) {
            echo 'Ваши данные были успешно изменены';;
        } else echo 'Ошибка' . $db->error;

        $stmt->close();
    }