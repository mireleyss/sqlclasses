<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
    <form action="write_data.php" method="post">
        <p>
            <label for="name">Имя</label>
            <input type="text" name="name" id="name" required>
        </p>
        <p>
            <label for="age">Возраст</label>
            <input type="number" name="age" id="age" required>
        </p>
        <p>
            <label for="email">Email</label>
            <input type="email" name="email" id="email" required>
        </p>

        <button type="submit" name="submit">Отправить</button>
    </form>

    <form action="" method="post">
        <p>
            <label for="id">ID</label>
            <input type="number" name="id" id="id" required>
        </p>

        <button type="submit" name="select">Отправить</button>
        <button type="submit" name="delete">Удалить пользователя</button>
        <button type="submit" name="change_data">Изменить данные</button>
    </form>

</body>
</html>


    <?php
        include_once "./connect.php";
        include_once "./write_data.php";
        include_once "./select_user.php";
        include_once "./delete_user.php";
        include_once "./update_data.php";

        $db->close();
    ?>