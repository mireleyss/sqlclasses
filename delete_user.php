<?php

    global $db;
    if ($_SERVER['REQUEST_METHOD'] == 'POST' and isset($_POST['delete'])) {
        $userID = $_POST['id'];
        deleteUser($db, $userID);
    }

    function deleteUser($db, $userID) {
        echo $userID;
        $query = "DELETE FROM user_data WHERE id = ?";

        $stmt = $db->prepare($query);

        $stmt->bind_param('i', $userID);

        if ($stmt->execute()) {
            echo 'Ваши данные были успешно удалены';;
        } else {
            echo 'Ошибка' . $db->error;
        }

        $stmt->close();
    }