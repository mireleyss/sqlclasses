<?php

    global $db;
    if ($_SERVER['REQUEST_METHOD'] == 'POST' and isset($_POST['select'])) {
        $userID = $_POST['id'];
        selectUser($db, $userID);
    }

    function selectUser($db, $userID) {

        $query = "SELECT * FROM user_data WHERE ID = ?";

        $stmt = $db->prepare($query);

        $stmt->bind_param('i', $userID);

        if ($stmt->execute()) {
            $result = $stmt->get_result();
        } else {
            echo 'Ошибка' . $db->error;
            exit();
        }

        $row = $result->fetch_assoc();

        print_r($row);

        $stmt->close();
    }




