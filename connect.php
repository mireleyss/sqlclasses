<?php

    $hostname = 'localhost';
    $username = 'root';
    $password = '';
    $database = 'users';

    global $db;
    $db = new mysqli($hostname, $username, $password, $database);

    if ($db->connect_errno) {
        echo 'Failed to connect to database' . $db->connect_errno;
        exit();
    }